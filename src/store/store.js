import { createStore } from "vuex"

const getBlocksSum = (arr) => {
    return arr.reduce((sum, block) => {
        return sum +=  block.offset > 1 ? Math.floor(block.weight / (1 + block.offset/10)) : block.weight
    }, 0)
}

const createBlock = () => {
    const type = Math.floor(Math.random() * 3);
    const weight = Math.floor(Math.random() * 10) + 1;
    const offset = Math.floor(Math.random() * 5) + 1;
    return {
        type,
        weight,
        offset
    };
}

const store = createStore({
    state: {
        leftBlocks: [],
        rightBlocks:[],
        nextLeftBlocks: [],
        nextRightBlocks: [],
        paused: true
    },
    getters: {
        sumLeft(state) {
            return getBlocksSum(state.leftBlocks)
        },
        sumRight(state) {
            return getBlocksSum(state.rightBlocks)
        },
        inclineDegree(state, getters) {
            const {sumLeft, sumRight} = getters;

            if (!sumLeft) return 0;
            if (sumLeft === sumRight) return 0;
            return sumLeft > sumRight ? (sumLeft - sumRight) / sumLeft * -100 : (sumRight - sumLeft) / sumRight * 100
        },
        gameOver(state, getters) {
            const {inclineDegree, sumLeft, sumRight} = getters
            return inclineDegree > 30 || inclineDegree < -30 || Math.abs(sumLeft - sumRight) > 20
        }
    },
    mutations: {
        togglePaused(state) {
            state.paused = !state.paused
        },
        addRightBlock(state) {
            const block = state.nextRightBlocks.shift()
            state.rightBlocks.push(block)
        },
        addLeftBlock(state) {
            const block = state.nextLeftBlocks.shift()
            state.leftBlocks.push(block)
        },
        addNextLeftBlock(state){
            const block = createBlock()
            state.nextLeftBlocks.push(block)
        },
        addNextRightBlock(state){
            const block = createBlock()
            state.nextRightBlocks.push(block)
        },
        setNextLeftBlocks(state){
            state.nextLeftBlocks = [];
            for(let i = 0; i < 2; i++){
                let block = createBlock()
                state.nextLeftBlocks.push(block)
            }
        },
        setNextRightBlocks(state){
            state.nextRightBlocks = [];
            for(let i = 0; i < 2; i++){
                let block = createBlock()
                state.nextRightBlocks.push(block)
            }
        },
        
        toLeft(state){
            if (state.paused || state.nextLeftBlocks[0].offset - 1 < 1) return
            state.nextLeftBlocks[0].offset -= 1
        },
        toRight(state){
            if (state.paused || state.nextLeftBlocks[0].offset + 1 > 5) return
            state.nextLeftBlocks[0].offset += 1
        },
        reset(state) {
            state.paused = true
            state.leftBlocks.length = 0
            state.rightBlocks.length = 0
            state.nextLeftBlocks.length = 0
            state.nextRightBlocks.length = 0
        }
    },
    actions: {
        reachedBottom({commit, getters}) {
            commit('addLeftBlock')
            commit('addRightBlock')
            commit('addNextLeftBlock')    
            commit('addNextRightBlock')    
            if (getters.gameOver)  {
                
                commit('togglePaused')
            }
        },
        start({commit}){
            commit('reset')
            commit('setNextLeftBlocks')
            commit('setNextRightBlocks')
        }
    }
})

export default store